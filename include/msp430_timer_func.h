/******************************************************************************
 * @ file  msp430_timer_func.h
 *
 *  Created on: 31/10/2018
 *      Author: AntoMota
 ******************************************************************************/

#ifndef __MSP430_TIMER_FUNC_H__
#define __MSP430_TIMER_FUNC_H__

/* Libraries to be included */
#include "msp430g2553.h"
#include <stdint.h>

/* Definitions */

/* Structs */

/* typedef struct {
	uint8_t CLK;
	uint8_t BR0;
	uint8_t BR1;
	uint8_t MODUL;	
}uart_struct; */

/* Function prototypes of the source file this header file references to */
uint8_t timer_conf();                   /* Setup timer */
uint8_t timer_wait();                   /* Timer delay (default) */
uint8_t timer_wait_def(uint16_t time);  /* Timer delay for the specified time*/


#endif /* __MSP430_TIMER_FUNC_H__ */