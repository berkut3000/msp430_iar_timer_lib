/*
 * msp430_conf.h
 *
 *  Created on: 17/10/2018
 *      Author: AntoMota
 */

#ifndef __MSP430_CONF_H__
#define __MSP430_CONF_H__

/* Libraries to be included */
#include "msp430g2553.h"
#include <stdint.h>

/* Definitions */
#define wake(x)	__bic_SR_register_on_exit(x)

/* Function prototypes of the source file this header file references to */
uint8_t msp430_conf();

uint8_t hab_in_rx();
uint8_t des_in_rx();

uint8_t hab_in_tmr();
uint8_t des_in_tmr();

uint8_t hab_in_P1(uint8_t gpio);
uint8_t des_in_P1(uint8_t gpio);

void dormir(uint8_t mode);
/* void wake(uint8_t mode); */

#endif /* __MSP430_CONF_H__ */
