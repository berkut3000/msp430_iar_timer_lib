/******************************************************************************
 * @ file  msp430_timer_func.c
 *
 *  Created on: 25/10/2018
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include "msp430_timer_func.h"
#include <stdint.h>

/* Definitions */
#define SUCCESS 0

/***************************************************************************//**
 * @brief
 *   Sets up the timer.
 *
 * @details
 *   Sets the the timer A0 up for the specified values.
 *
 * @note
 *   Read the datasheet for the different available timers.
 *
 * @param[in] src_clk
 *   The source clock which will control the timer.
 *
 * @param[in] timer_mode
 *   The mode the Timer will operate like.
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint8_t timer_conf()
{
    TACTL = TASSEL_2 + MC_2;                  // SMCLK, contmode
    return SUCCESS;
}


/***************************************************************************//**
 * @brief
 *   Delay for approximately one second.
 *
 * @details
 *   Performs a system delay for one second while in LPMO mode,
 *      so there's low-consumption.
 *
 * @note
 *   Adjust the value to be compared according to your src_clk configuration.
 *      For this example is for one second
 *
 * @param[in] none
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint8_t timer_wait()
{
    uint16_t count_timer = 0;
    uint16_t clk[2] = {0xFFFF,0xFFFF};
    uint16_t time = 0;
    
    clk[0] = BCSCTL1;
    clk[1] = DCOCTL; 
    
    if((CALBC1_1MHZ == clk[0]) & (CALDCO_1MHZ == clk[1]))
    {
        /* time = 1000/2; */
        time = 15;
        
    }else if((CALBC1_16MHZ == clk[0]) & (CALDCO_16MHZ == clk[1])){
        /* time = 16000/2; */
        time = 245;
    }else{
        time = 15;
    }
    
    while (count_timer < time)
    {
        CCTL0 |= CCIE;//Funci�n para habilitar interrupciones TIMERA0
        _BIS_SR(LPM0_bits + GIE);                 // Enter LPM0 w/ interrupt
        count_timer++;
        CCTL0 &= ~CCIE;;//Funci�n para deshabilitar interrupciones TIMERA0
    }
    count_timer = 0;
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   Delay for the specified time.
 *
 * @details
 *   Performs a system delay while in LPMO mode for the specified time,
 *      so there's low-consumption.
 *
 * @note
 *   Adjust the value to be compared according to your src_clk configuration.
 *      For this example is for one second.
 *
 * @param[in] time
 *   Time specified to be delayed.
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint8_t timer_wait_def(uint16_t time)
{
    uint16_t count_timer = 0;    
    while (count_timer < time)
    {
        CCTL0 |= CCIE;//Funci�n para habilitar interrupciones TIMERA0
        _BIS_SR(LPM0_bits + GIE);                 // Enter LPM0 w/ interrupt
        count_timer++;
        CCTL0 &= ~CCIE;//Funci�n para deshabilitar interrupciones TIMERA0
    }
    count_timer = 0;
    return SUCCESS;
}