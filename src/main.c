/******************************************************************************
 * @ file  main.c
 *
 * @brief
 *  Simple demonstration of the timer function.    
 *
 * @details
 *  Demonstration of the MSP430G2553 TIMERA0 functions. A simple delay is
 *      is generated using the "timer_wait" and "timer_wait_def" function calls.
 *
 *
 *  Created on: 25/10/2018
 *      Author: AntoMota
 ******************************************************************************/

#include "msp430g2553.h"
#include "uart_func.h"
#include "msp430_conf.h"
#include "msp430_timer_func.h"
#include <stdint.h>

void SwapChars(uint8_t *a, uint8_t *b);

volatile uint8_t Data;

void main(void)
{
    /* uint8_t bufferghini[20]; */
    /* uint8_t * p_buff; */
    /* uint8_t x1 = '0'; */
    /* uint8_t x2 = '2'; */
    
    msp430_conf();
    
    while(1)
    {
        /* timer_wait(); */
        /* timer_wait(); */
        timer_wait_def(10);
        P1OUT ^= 0x01;
        
    }
}


#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
  Data = UCA0RXBUF;
  P1OUT ^= BIT0;
  __bic_SR_register_on_exit(LPM0_bits);
}

// Rutina de interrupción de temporizador (TIMERA0)
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
  /* P1OUT ^= 0x01;                            // Toggle P1.0 */
  CCR0 += 65535;                            // Add Offset to CCR0
  /* CCR0 += 60000; */
  wake(LPM0_bits);
}

void SwapChars(uint8_t *a, uint8_t *b)
{
    uint8_t c = 0;      // initialise temporary variable c
    P1OUT |= BIT6;            // Set P1.0
    c = *a;      // copy the value in memory location a in variable c
    *a = *b;     // copy the value stored in memory location b into memory location a
    *b = c;      // copy the value temporarily stored in c into memory location b
}
