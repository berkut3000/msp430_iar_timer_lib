/******************************************************************************
 * @ file  msp430_conf.c
 *
 *  Created on: 25/10/2018
 *      Author: AntoMota
 ******************************************************************************/

/* Definition */
#define SUCCESS 0
 
/* Headers */
#include "msp430_conf.h"
#include "uart_func.h"
#include "msp430_timer_func.h"

/* Functions */

/***************************************************************************//**
 * @brief
 *   Configuration of MSP430.
 *
 * @details
 *   Custom configuration per project is defined in this function.
 *
 * @note
 *   Clock is (must) be defined here.
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint8_t msp430_conf()
{
    
    uint16_t clk[2] = {0xFFFF,0xFFFF};
    uint16_t retencion = 0x00C1;
    WDTCTL = WDTPW + WDTHOLD; // Stop WDT
    P1DIR |= BIT0;    // Set the LEDs on P1.0, P1.6 as outputs
    // P2DIR |= BIT0;
    P1OUT |= BIT0;            // Set P1.0
    P1OUT &= ~BIT0;
    
    BCSCTL1 = CALBC1_1MHZ;   // Set DCO to 1MHz
    DCOCTL = CALDCO_1MHZ;    // Set DCO to 1MHz
    
    timer_conf();
    
    return SUCCESS;
}


/***************************************************************************//**
 * @brief
 *   Enable UART reception Interrupts.
 *
 * @details
 *   Enable the UART RX Interrupt from the Interrupts Vector Register.
 *
 * @note
 *   In this example, the GPIO Interrupts are disabled.
 *
 * @param[in] none.
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t hab_in_rx()
{
    IE2 |= UCA0RXIE;
    /* P1IE &= ~SW;  */
	return 0;
}

/***************************************************************************//**
 * @brief
 *   Disables UART recpetion Interrupts
 *
 * @details
 *   Disables the UART RX Interrupt from the Interrupts Vector Register.
 *
 * @note
 *   In this example, GPIO Pins in SW are enabled.
 *
 * @param[in] none.
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t des_in_rx()
{
    IE2 &= ~UCA0RXIE;
    /* P1IE |= SW; // P1.3 interrupt enabled */
	return 0;
}

/***************************************************************************//**
 * @brief
 *   Enables TIMERA0 Interrupts.
 *
 * @details
 *   Enables TIMERA0 Interrupts from CCIE Register.
 *
 * @note
 *   none.
 *
 * @param[in] none
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t hab_in_tmr()
{
    CCTL0 |= CCIE;
	return 0;
}

/***************************************************************************//**
 * @brief
 *   Disables TIMERA0 Interrupts.
 *
 * @details
 *   Disables TIMERA0 Interrupts from CCIE Register.
 *
 * @note
 *   none.
 *
 * @param[in] none
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t des_in_tmr()
{
    CCTL0 &= ~CCIE;
	return 0;
}

/***************************************************************************//**
 * @brief
 *   Enable GPIO1 Interrupts.
 *
 * @details
 *   Enable the GPIO1 Interrupts from the Interrupts Vector Register, related
 *		to the specified pin.
 *
 * @note
 *
 *
 * @param[in] gpio.
 *	Specified GPIO Pin Number
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t hab_in_P1(uint8_t gpio)
{
    P1IE |= gpio;
	return 0;
}

/***************************************************************************//**
 * @brief
 *   Enable GPIO1 Interrupts.
 *
 * @details
 *   Enable the GPIO1 Interrupts from the Interrupts Vector Register, related
 *		to the specified pin.
 *
 * @note
 *
 *
 * @param[in] gpio.
 *	Specified GPIO Pin Number
 *
 * @return
 *   0.
 ******************************************************************************/
uint8_t des_in_P1(uint8_t gpio)
{
    P1IE &= ~gpio;
	return 0;
}

/***************************************************************************//**
 * @brief
 *	Sets the MCU to sleep.
 *
 * @details
 *	Sets the MCU to Low-Power Mode with Interrupts enabled.
 *
 * @note
 *	LPMO, LPM2, LPM3 and LPM4 are acceptable Power Modes,  refer to page 39
 *	of the slau144 document (MSP430 Family User's Guide) for detailed info
 *	about any of the aforementioned modes.
 *
 * @param[in] mode
 *   LPMX Low-Power Mode.
 *
 * @return
 *   Nothing.
 ******************************************************************************/
void dormir(uint8_t mode)
{
    __bis_SR_register(mode + GIE); // Enter LPM0, interrupts enabled
}

/***************************************************************************//**
 * @brief
 *	Wakes up the MCU.
 *
 * @details
 *	Wakes the MCU from the Low-Power Mode.
 *
 * @note
 *	LPMO, LPM2, LPM3 and LPM4 are acceptable Power Modes,  refer to page 39
 *	of the slau144 document (MSP430 Family User's Guide) for detailed info
 *	about any of the aforementioned modes.
 *
 * @param[in] mode
 *   LPMX Low-Power Mode.
 *
 * @return
 *   Nothing.
 ******************************************************************************/
/* void wake(uint8_t mode)
 {
	 __bic_SR_register_on_exit(mode);
 } */
